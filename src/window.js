/* window.js
 *
 * Copyright 2020 Yan Babilliot
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written
 * authorization.
 */

const { GObject, Gio, Gtk } = imports.gi;

const utils = imports.utils;

var Window = GObject.registerClass({
    GTypeName: 'Window',
    Template: 'resource:///org/gnome/Appearance/window.ui',
    InternalChildren: [
        'stack', 'stack_interface', 'stack_dock', 'stack_panel',
        'enable_hot_corners', 
        'clock_weekday', 'clock_date', 'clock_seconds', 
        'calendar_weekdate',
        'menu_button'
    ]
}, class AppearanceWindow extends Gtk.ApplicationWindow {

    _init(application) {

        super._init({ application });

        this.settings = utils.getSettings('org.gnome.Appearance');
        this.interfaceSettings  = utils.getSettings('org.gnome.desktop.interface');
        this.calendarSettings   = utils.getSettings('org.gnome.desktop.calendar');

        this.init_window();
        this.init_stack();
        this.init_stack_panel();
    
    }

    init_window() {
    }

    init_stack() {
        // Track Stack state
        let stack = this.settings.get_string('stack');
        this._stack.set_visible_child(this['_stack_' + stack]);
        this._stack.connect('notify::visible-child', (stack) => {
            this.settings.set_string('stack', stack.visible_child_name)
        })
    }
    
    set_menu(gMenu) {
        this._menu_button.set_menu_model(gMenu);
    }
    
    /*
     * Panel
     */

    init_stack_panel() {
        this._bind(this.interfaceSettings, 'enable-hot-corners', this._enable_hot_corners)
        this._bind(this.interfaceSettings, 'clock-show-weekday', this._clock_weekday)
        this._bind(this.interfaceSettings, 'clock-show-weekday', this._clock_weekday)
        this._bind(this.interfaceSettings, 'clock-show-date', this._clock_date)
        this._bind(this.interfaceSettings, 'clock-show-seconds', this._clock_seconds)
        this._bind(this.calendarSettings,  'show-weekdate', this._calendar_weekdate)
    }

    _bind(settings, key, widget, type = 'boolean') {
        widget.set_active(settings['get_' + type](key))
        settings.bind(key, widget, 'active', Gio.SettingsBindFlags.DEFAULT);
    }

});

