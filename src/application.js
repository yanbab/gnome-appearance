/* application.js */

const { GObject, Gio, Gtk }  = imports.gi;

const { Window } = imports.window;

var Application = GObject.registerClass(
class Application extends Gtk.Application {

    _init(application_id) {
        super._init({ application_id });
    }

    vfunc_activate() {
        if (!this._window) {
            this._window = new Window(this);
            this._window.set_menu(this.get_menu());
        }
        this._window.show_all();
        this._window.present();
    }

    get_menu() {

        let menu = new Gio.Menu();
        let action = null;

        menu.append("About", 'app.about');
        action = new Gio.SimpleAction ({ name: 'about' });
        action.connect('activate', () => { this.about(); });
        this.add_action(action);

        menu.append("Quit",'app.quit');
        action = new Gio.SimpleAction ({ name: 'quit' });
        action.connect('activate', () => { this.quit(); });
        this.add_action(action);

        return menu;
        
    }

    about() {
        let aboutDialog = new Gtk.AboutDialog({
            authors: [
                '<a href="https://gitlab.com/yanbab">Yan Babilliot</a>',
            ],
            translator_credits: _('translator-credits'),
            program_name: _('Appearance'),
            comments: _('Customize your desktop'),
            license_type: Gtk.License.GPL_3_0,
            logo_icon_name: 'org.gnome.Appearance',
            version: imports.package.version,
            transient_for: this._window,
            modal: true,
        });
        aboutDialog.present();
    }


});