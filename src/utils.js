/*
 * utils.js
 */

const { Gio } = imports.gi;

function getSettings(schema_id) {
    let settings = null;
    try {
        settings = new Gio.Settings({schema_id: schema_id});
    } catch(e) {
        print("Warning: schema '" + schema_id + "' not found");
    }
    return settings;
}
